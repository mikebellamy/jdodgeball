// Copyright Epic Games, Inc. All Rights Reserved.

#include "JDodge.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, JDodge, "JDodge" );

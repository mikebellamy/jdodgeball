// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "JDodgeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class JDODGE_API AJDodgeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
